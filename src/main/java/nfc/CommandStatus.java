package nfc;

public enum CommandStatus {
    OK, TIMEOUT, INVALID_ACK
}