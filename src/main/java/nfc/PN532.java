package nfc;

import java.util.Random;

public class PN532 {

    static final byte PN532_COMMAND_GETFIRMWAREVERSION = 0x02;
    static final byte PN532_COMMAND_SAMCONFIGURATION = 0x14;
    static final byte PN532_COMMAND_INLISTPASSIVETARGET = 0x4A;
    static final byte PN532_COMMAND_INDATAEXCHANGE = 0x40;

    static final byte TG_INIT_AS_TARGET = (byte) 0x8C;

    private byte inListedTag;

    IPN532Interface medium;
    private byte[] pn532_packetbuffer;

    public PN532(IPN532Interface medium) {
        this.medium = medium;
        this.pn532_packetbuffer = new byte[64];
    }

    public void begin() {
        medium.begin();
        medium.wakeup();
    }

    public IPN532Interface getMedium() {
        return this.medium;
    }

    public long getFirmwareVersion() throws InterruptedException {
        long response;

        byte[] command = new byte[1];
        command[0] = PN532_COMMAND_GETFIRMWAREVERSION;

        if (medium.writeCommand(command) != CommandStatus.OK) {
            return 0;
        }

        // read data packet
        int status = medium.readResponse(pn532_packetbuffer, 22);
        if (status < 0) {
            return 0;
        }

        int offset = 0; //medium.getOffsetBytes();

        response = pn532_packetbuffer[offset + 0];
        response <<= 8;
        response |= pn532_packetbuffer[offset + 1];
        response <<= 8;
        response |= pn532_packetbuffer[offset + 2];
        response <<= 8;
        response |= pn532_packetbuffer[offset + 3];

        return response;
    }

    public boolean SAMConfig() throws InterruptedException {
        byte[] command = new byte[4];
        command[0] = PN532_COMMAND_SAMCONFIGURATION;
        command[1] = 0x01; // normal mode;
        command[2] = 0x14; // timeout 50ms * 20 = 1 second
        command[3] = 0x01; // use IRQ pin!

        if (medium.writeCommand(command) != CommandStatus.OK) {
            return false;
        }

        return medium.readResponse(pn532_packetbuffer, 8) > 0;
    }

    public boolean inDataExchange(byte[] adpu, int adpuLength, byte[] response, int responseLength) throws InterruptedException {
        byte[] command = new byte[2];
        command[0] = 0x40; //PN532_COMMAND_INDATAEXCHANGE
        command[1] = inListedTag;

        if (medium.writeCommand(command) != CommandStatus.OK) {
            return false; // command failed
        }

        int status = medium.readResponse(response, responseLength, 1000);
        if (status < 0) {
            return false;
        }

        if ((response[0] & 0x3f) != 0) {
            System.err.println("Status code indicates an error\n");
            return false;
        }

        int length = status;
        length -= 1;

        if(length > responseLength) {
            length = responseLength;
        }

        for(int i = 0; i < length; i++) {
            response[i] = response[i + 1];
        }

        responseLength = length;

        return true;
    }

    public boolean inListPassiveTarget() throws InterruptedException {
        byte[] command = new byte[3];

        command[0] = PN532_COMMAND_INLISTPASSIVETARGET;
        command[1] = 1;
        command[2] = 0;

        if(medium.writeCommand(command) != CommandStatus.OK) {
            return false;
        }

        int status = medium.readResponse(pn532_packetbuffer, pn532_packetbuffer.length, 30000);
        if(status < 0) return false;

        if(pn532_packetbuffer[0] != 1) return false;

        inListedTag = pn532_packetbuffer[1];

        return true;
    }

    public int readPassiveTargetID(byte cardbaudrate, byte[] buffer, boolean listTag) throws InterruptedException {
        byte[] command = new byte[3];
        command[0] = PN532_COMMAND_INLISTPASSIVETARGET;
        command[1] = 1; // max 1 cards at once (we can set this to 2 later)
        command[2] = (byte) cardbaudrate;

        if (medium.writeCommand(command) != CommandStatus.OK) {
            return -1; // command failed
        }

        // read data packet
		if (medium.readResponse(pn532_packetbuffer, pn532_packetbuffer.length) < 0) {
        //if (medium.readResponse(pn532_packetbuffer, 23) < 0) {
            return -1;
        }

        // check some basic stuff
        /*
         * ISO14443A card response should be in the following format:
         *
         * byte Description -------------
         * ------------------------------------------ b0 Tags Found b1 Tag
         * Number (only one used in this example) b2..3 SENS_RES b4 SEL_RES b5
         * NFCID Length b6..NFCIDLen NFCID
         */

        int offset = 0; //medium.getOffsetBytes();

        if (pn532_packetbuffer[offset + 0] != 1) {
            return -1;
        }
        // int sens_res = pn532_packetbuffer[2];
        // sens_res <<= 8;
        // sens_res |= pn532_packetbuffer[3];

        // DMSG("ATQA: 0x"); DMSG_HEX(sens_res);
        // DMSG("SAK: 0x"); DMSG_HEX(pn532_packetbuffer[4]);
        // DMSG("\n");

        /* Card appears to be Mifare Classic */
        int uidLength = pn532_packetbuffer[offset + 5];

        for (int i = 0; i < uidLength; i++) {
            buffer[i] = pn532_packetbuffer[offset + 6 + i];
        }

        if(listTag) {
            inListedTag = pn532_packetbuffer[1];
        }

        return uidLength;
    }

}