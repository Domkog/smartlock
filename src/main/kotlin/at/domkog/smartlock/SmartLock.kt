@file:Suppress("INCOMPATIBLE_ENUM_COMPARISON")

package at.domkog.smartlock

import at.domkog.smartlock.db.sqlite.SQLiteDatabase
import at.domkog.smartlock.user.UserController
import nfc.IPN532Interface
import nfc.PN532
import nfc.PN532I2C

fun main() {
    val db = SQLiteDatabase(getDBDir())
    db.connect()
    val userController = UserController(db)
    userController.initTable()

    val PN532_MIFARE_ISO14443A: Byte = 0x00

    val pn532Interface: IPN532Interface = PN532I2C();
    val nfc: PN532 = PN532(pn532Interface);

    // Start
    println("Starting up...");
    nfc.begin();
    Thread.sleep(1000);

    val versiondata = nfc.firmwareVersion
    if (versiondata == 0L) {
        println("Didn't find PN53x board")
        return
    }

    // Got ok data, print it out!
    print("Found chip PN5")
    println(java.lang.Long.toHexString(versiondata shr 24 and 0xFF))

    print("Firmware ver. ")
    print(java.lang.Long.toHexString(versiondata shr 16 and 0xFF))
    print('.')
    println(java.lang.Long.toHexString(versiondata shr 8 and 0xFF))

    nfc.SAMConfig();

    println("Waiting for an ISO14443A Card ...")


    while(true) {
        var success = false;
        val responseLength = 32;

        println("Waiting for an ISO14443A card...");

        success = nfc.inListPassiveTarget();

        if(success) {
            println("Found something!");

            val selectApdu = getApduArray();

            val response = ByteArray(32);
            success = nfc.inDataExchange(selectApdu, selectApdu.size, response, response.size);

            if(success) {
                println("responseLength: $responseLength");
                printAsHex(response);
            }
        }

        Thread.sleep(100);
    }

    /*val buffer = ByteArray(8)
    while (true) {
        val readLength = nfc.readPassiveTargetID(
            PN532_MIFARE_ISO14443A,
            buffer,
            false
        )

        if (readLength > 0) {
            println("Found an ISO14443A card")

            print("  UID Length: ")
            print(readLength)
            println(" bytes")

            print("  UID Value: [")
            for (i in 0 until readLength) {
                print(Integer.toHexString(buffer[i].toInt()))
            }
            println("]")
            Thread.sleep(10000);
        }

        Thread.sleep(250)
    }*/

}

fun getApduArray(): ByteArray {
    val selectApdu = ByteArray(13);
    selectApdu[0] = 0x00;
    selectApdu[1] = 0xA4.toByte();
    selectApdu[2] = 0x04;
    selectApdu[3] = 0x00;
    selectApdu[4] = 0x07;

    selectApdu[5] = 0xF0.toByte();
    selectApdu[6] = 0x01;
    selectApdu[7] = 0x02;
    selectApdu[8] = 0x03;
    selectApdu[9] = 0x04;
    selectApdu[10] = 0x05;
    selectApdu[11] = 0x06;

    selectApdu[12] = 0x00;

    return selectApdu;
}

fun printAsHex(array: ByteArray) {
    for(i in array.indices) {
        println("");
        print(Integer.toHexString(array[i].toInt()));
        println("");
    }
}

fun getDBDir(): String {
    val home = System.getProperty("user.home");
    return "$home/SmartLock";
}
