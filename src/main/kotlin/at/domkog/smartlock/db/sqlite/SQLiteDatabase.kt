package at.domkog.smartlock.db.sqlite

import at.domkog.smartlock.db.Database
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.util.concurrent.CompletableFuture

class SQLiteDatabase(private val dbDir: String) : Database {
    lateinit var connection: Connection;

    override fun connect() {
        Class.forName("org.sqlite.JDBC");

        val dbFile = getDBFile();
        if(!dbFile.exists()) {
            dbFile.createNewFile();
        }
        this.connection = DriverManager.getConnection("jdbc:sqlite:" + getDBFile().absolutePath);
    }

    override fun disconnect() {
        connection.close();
    }

    override fun execute(query: String) {
        GlobalScope.launch {
            val statement = connection.createStatement();
            statement.execute(query);
            statement.close()
        }
    }

    override fun executeQuery(query: String): CompletableFuture<ResultSet> {
        return CompletableFuture.supplyAsync {
            val statement = connection.createStatement();
            val rs = statement.executeQuery(query);
            return@supplyAsync rs;
        };
    }

    override fun getDBFile(): File {
        val parent = File(this.dbDir);
        if(!parent.exists()) {
            parent.mkdirs()
        }
        return File(parent, "SmartLock.db")
    }

}
