package at.domkog.smartlock.db

import java.io.File
import java.sql.ResultSet
import java.util.concurrent.CompletableFuture

interface Database {

    fun connect()
    fun disconnect()

    fun execute(query: String)
    fun executeQuery(query: String): CompletableFuture<ResultSet>

    fun getDBFile(): File

}
