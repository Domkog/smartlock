package at.domkog.smartlock.db

import java.util.concurrent.CompletableFuture
import java.util.concurrent.Future

interface DBController<T> {

    fun initTable()

    fun add(obj: T)
    fun remove(obj: T)
    fun update(obj: T)

    fun getAll(query: String): CompletableFuture<List<T>>
    fun getOne(query: String): CompletableFuture<T>

}