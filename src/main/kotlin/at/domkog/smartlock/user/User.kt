package at.domkog.smartlock.user

data class User(val uuid: String, var accessLevel: Int)