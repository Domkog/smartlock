package at.domkog.smartlock.user

import at.domkog.smartlock.db.DBController
import at.domkog.smartlock.db.Database
import java.util.concurrent.CompletableFuture

class UserController(private val database: Database) : DBController<User> {

    override fun initTable() {
        database.execute("CREATE TABLE IF NOT EXISTS users (uuid TEXT PRIMARY KEY, accessLevel INTEGER NOT NULL);");
    }

    override fun add(obj: User) {
        database.execute("INSERT INTO users (uuid, accessLevel) VALUES ('${obj.uuid}', '${obj.accessLevel}');");
    }

    override fun remove(obj: User) {
        database.execute("DELETE FROM users WHERE uuid = '${obj.uuid}';");
    }

    override fun update(obj: User) {
        database.execute("UPDATE users SET accessLevel = '${obj.accessLevel}' WHERE uuid = '${obj.uuid}';");
    }

    override fun getAll(query: String): CompletableFuture<List<User>> {
        return database.executeQuery(query).thenApply {rs ->
            val list = mutableListOf<User>();
            while(rs.next()) {
                list.add(User(rs.getString("uuid"), rs.getInt("accessLevel")));
            }
            rs.close();
            return@thenApply list.toList();
        }
    }

    override fun getOne(query: String): CompletableFuture<User> {
        return database.executeQuery(query).thenApply {rs ->
            var user: User? = null
            if(rs.next()) user = User(rs.getString("uuid"), rs.getInt("accessLevel"));
            rs.close();
            return@thenApply user;
        }
    }

}